import enUS from './en-us'
import vi from './vi'
import ko from './korean'

export default {
  'en-us': enUS,
  vi : vi,
  ko:ko
}

