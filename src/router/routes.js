const routes = [
  {
    path: '/',
    component: () => import('layouts/ClientLayout.vue'),
    children: [
      { path: '/', component: () => import('pages/client/Home.vue') },
      { path: 'product', component: () => import('pages/client/Product.vue') },
      { path: 'article', component: () => import('pages/client/ArticleList.vue') },
    ]
  
  }
 
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
    routes.push({
        path: '*',
        component: () =>
            import ('pages/Error404.vue')
    })
}

export default routes