import request from './index'

export function loadArticleAll () {
  return request({
    url: '/api/index.php/v1/content/article',
    method: 'get'
  })
}

export function getArticleById (aId) {
  return request({
    url: '/api/index.php/v1/content/article/' + aId,
    method: 'get'
  })
}

export function deleteArticleById (aId) {
  return request({
    url: '/api/index.php/v1/content/article/' + aId,
    method: 'delete'
  })
}
export function postArticle (aticle) {
  return request({
    url: '/api/index.php/v1/content/article',
    method: 'post',
    data: aticle
  })
}
export function putArticle (id, aticle) {
  return request({
    url: '/api/index.php/v1/content/article/' + id,
    method: 'patch',
    data: aticle
  })
}
