import axios from 'axios'
import { Loading } from 'quasar'
let loadFunction = config => {
  Loading.show()
  return config
}
let finishFunction = response => {
  Loading.hide()
  return response
}
let errorFunction = error => {
  Loading.hide()
  return Promise.reject(error)
}

const service  = axios.create({
    baseURL: process.env.API, // api的base_url
    timeout: 20000, // request timeout
     headers : { 
      'Authorization': 'Bearer ' + 'c2hhMjU2OjUyOmY0MTQ5NzQ4MDk4NWRmZGE1YzJhMjRjMzBkODBjNDg1NTEyYTdmODU0MzM5OWY0MDA0ZDJkZGFiYmUxMGQ0NWU='
     }
})

service.interceptors.request.use(loadFunction)
service.interceptors.response.use(finishFunction, errorFunction)

export default service