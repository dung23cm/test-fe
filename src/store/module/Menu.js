const state = {
        menu: [
            {
                name: '회원 관리',
                to: '/hym',
                subMenu: [{
                    name: '역할&권한 관리',
                    to: ''
                },
                {
                    name: '회원 관리',
                    to: '/hym/member/list'
                }
                ]
            },
            {
                name: '게시판 관리',
                to: '/hym',
                subMenu: [{
                        name: 'Press 게시판',
                        to: '/hym/post/press'
                    },
                    {
                        name: 'Video 게시판',
                        to: '/hym/post/video'
                    },
                    {
                        name: '문의 게시판',
                        to: '/hym',
                        subCont: [{
                            name: '주소',
                            to: '/hym/post/contact'
                        },
                        {
                            name: '메시지',
                            to: '/hym/post/message'
                        }
                    ]
                    }
                ]
            },
            {
                name: '리포트 관리',
                to: '/hym',
                subMenu: [{
                        name: '질문지 관리',
                        to: '/hym/report/question/list'
                    },
                    {
                        name: '응답지 관리',
                        to: '/hym/report/answer/list'
                    }
                ]
            },
            {
                name: '사이트 관리',
                to: '/hym',
                subMenu: [{
                        name: '메인화면 관리',
                        to: ''
                    },
                    {
                        name: '메뉴구성 관리',
                        to: ''
                    },
                    {
                        name: '페이지 관리',
                        to: ''
                    },
                    {
                        name: '사이트 설정',
                        to: ''
                    }
                ]
            }
        ],
    }
    // const mutations = {
    //     setApprovalMenu(state) {
    //         state.menu = state.approvalMenu
    //     },
    // }

export default {
    namespaced: true,
    state,
}